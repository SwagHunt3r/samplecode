//
// This class defines a picture frame with audio reactive detailing
// Utilizes FFT derived from https://github.com/DarthAffe/KeyboardAudioVisualizer/releases and chuckFFT, which was a simple conversion from C to C++
// 

#include "MoCapProjectJE.h"
#include "APictureFrame.h"


// Sets default values
APictureFrame::APictureFrame()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initial values
	m_fColorLow[0] = 0;
	m_fColorLow[1] = 0.42;
	m_fColorLow[2] = 1;

	m_fColorHigh[0] = 0;
	m_fColorHigh[1] = 0.42;
	m_fColorHigh[2] = 1;
}

// Called when the game starts or when spawned
void APictureFrame::BeginPlay()
{
	Super::BeginPlay();
	
	// Get reference to the game mode
	m_pGameMode = (AVisualizerMode*)GetWorld()->GetAuthGameMode();

	// Standard method for acquiring handles to objects in the scene
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		FString name = ActorItr->GetName();
		if (name.Contains(TEXT("audioreact")))
		{
			// Get the numeric portion of the name
			name = name.Mid(10, 3);
			int num = FCString::Atoi(*name) - 1;

			// Store actor in vectors, all actors with the same designated number take input from the same audio frequency
			m_pAudioReactParts[num].Push(*ActorItr);
			// Store the original scale values from the editor, so each item reacts correctly
			m_pPartScale[num].Push(ActorItr->GetActorScale3D());
		}

		// Store the backing of the picture frame for hiding when desired
		if (name.Contains(TEXT("frameback")))
		{
			m_pFrameBackParts.Push(*ActorItr);
		}
	}

	// Acquire the materials for each of the objects
	for (int a = 0; a < 128; a++)
	{
		// Repeat for each object with the same name
		for (int b = 0; b < m_pAudioReactParts[a].Num(); b++)
		{
			TArray<UStaticMeshComponent*> Components;

			// Get the mesh component
			m_pAudioReactParts[a][b]->GetComponents<UStaticMeshComponent>(Components);

			// For each mesh contained in the actor
			for (int c = 0; c < Components.Num(); c++)
			{
				UStaticMeshComponent* StaticMeshComponent = Components[c];
				UMaterialInstanceDynamic* pCurMaterial = StaticMeshComponent->CreateAndSetMaterialInstanceDynamic(0);

				// Set default material parameters
				pCurMaterial->SetVectorParameterValue(TEXT("EmissiveColor"), FVector(0.0, 0.21, 0.5));
				pCurMaterial->SetScalarParameterValue(TEXT("Brightness"), 1);

				// Store material for later use
				m_pAudioReactMaterials[a].Push(pCurMaterial);
			}
		}
	}
}

// Toggle if the picture frame is visible
void APictureFrame::SetHidden()
{
	m_bHidden = !m_bHidden;

	// For each frequency
	for (int a = 0; a < 128; a++)
	{
		// For each item in the vector
		for (int b = 0; b < m_pAudioReactParts[a].Num(); b++)
		{
			// Set the hidden state of the actor
			m_pAudioReactParts[a][b]->SetActorHiddenInGame(m_bHidden);
		}
	}

	// Set the hidden state of the frame backing
	for (int a = 0; a < m_pFrameBackParts.Num(); a++)
		m_pFrameBackParts[a]->SetActorHiddenInGame(m_bHidden);
}

// Called every frame
void APictureFrame::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float fAudioScale; // Magnitude of volume for the current frequency
	short nCount = 0; // Steps through full 256 frequencies

	// Loop through half the available frequencies, averaged
	for (int a = 0; a < 128; a++)
	{
		fAudioScale = 0;

		// Total two frequencies
		for (int b = 0; b < 2; b++)
			fAudioScale += m_pGameMode->fft_fltr[nCount++]; // Get the scalar value for the current frequency from the game mode
		
		// Average of two frequencies
		fAudioScale /= 2;

		// Perform audio reaction for each mesh tied to this frequency
		for (int b = 0; b < m_pAudioReactParts[a].Num(); b++)
		{
			// Get the current scale of the actor
			FVector vScale = m_pAudioReactParts[a][b]->GetActorScale3D();

			// Minimum scale is .9 of the size displayed in the editor, maximum is 30% larger
			vScale.X = m_pPartScale[a][b].X * .9 + (m_pPartScale[a][b].X * .3) * fAudioScale;
			vScale.Y = m_pPartScale[a][b].Y * .9 + (m_pPartScale[a][b].Y * .3) * fAudioScale;
			
			// Set the new scale
			m_pAudioReactParts[a][b]->SetActorScale3D(vScale);

			float Low[3] = { m_fColorLow[0], m_fColorLow[1], m_fColorLow[2] };
			float High[3] = { m_fColorHigh[0], m_fColorHigh[1], m_fColorHigh[2] };
			float Result[3] = { 0 };

			// Scale the brightness based on user input
			for (int c = 0; c < 3; c++)
				Low[c] *= m_fBrightnessLow;
			for (int c = 0; c < 3; c++)
				High[c] *= m_fBrightnessHigh;

			// Interpolate between the low and high color, this is handled in the controller class
			FColor3Lerp(Low, High, Result, 10 * fAudioScale);

			// Set the material parameter, this is the color that is displayed
			m_pAudioReactMaterials[a][b]->SetVectorParameterValue(TEXT("EmissiveColor"), FVector(Result[0], Result[1], Result[2]));
		}
	}
}