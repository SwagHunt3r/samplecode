//
// Utilizes vector fields to generate a particle effect based on Kinect data
// Updates only during the Kinect's update cycle
//

#include "MoCapProjectJE.h"
#include "DynamicVectorFieldActor.h"


// Sets default values
ADynamicVectorFieldActor::ADynamicVectorFieldActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADynamicVectorFieldActor::BeginPlay()
{
	Super::BeginPlay();

	// Standard method for acquiring handles to objects in the scene
	for (TActorIterator<AVectorFieldVolume> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		FString name = ActorItr->GetName();

		// Store pointers to the vector fields
		if (name.Equals(TEXT("VectorFieldAlpha")))
			m_pVectorFieldActor = *ActorItr;
		else if (name.Equals(TEXT("VectorFieldAudio")))
			m_pVectorFieldActorAudio = *ActorItr;
	}

	// Extract the vector fields from the actors
	m_pVectorFieldComponent = m_pVectorFieldActor->GetVectorFieldComponent();
	m_pVectorField = (UVectorFieldStatic*)m_pVectorFieldComponent->VectorField;

	m_pVectorFieldComponentAudio = m_pVectorFieldActorAudio->GetVectorFieldComponent();
	m_pVectorFieldAudio = (UVectorFieldStatic*)m_pVectorFieldComponentAudio->VectorField;
}

// Called every frame
void ADynamicVectorFieldActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Automatic cycling through visual effects
	if (m_bEffectsAuto)
	{
		// Update the timer
		m_fNextEffect -= DeltaTime;

		// When the timer reaches 0
		if (m_fNextEffect <= 0)
		{
			// Cycle to a random effect
			m_nEffect = FMath::RandRange(0, m_nNumEffects);

			// Set the effect timer based on how long is needed for completion of the current effect
			if (m_nEffect == 0 || m_nEffect == 1)
				m_fEffectTimer = 12;
			else
				m_fEffectTimer = 10;

			// Set timer for next effect to play
			m_fNextEffect = FMath::RandRange(15, 45);
		}
	}
}

// This function is called by the game mode, when the Kinect data is ready to be used
// It updates the vector field data, which then effects the particles passed into it
void ADynamicVectorFieldActor::ProcessDepth(INT64 nTime, UINT16* pBuffer, int nWidth, int nHeight, USHORT nMinDepth, USHORT nMaxDepth)
{
	// Make sure we've received valid data
	if (pBuffer && m_bVisible)
	{
		FFloat16 fHeight = 0;
		nCount = 0;

		// End pixel is start + width * height - 1
		const UINT16* pBufferStart = pBuffer;
		const UINT16* pBufferEnd = pBuffer + (nWidth * nHeight);

		FFloat16 *source;

		int nMove = 0;
		float fAudioScale = 0;

		// Lock the vector field data for editing
		source = (FFloat16*)m_pVectorField->SourceData.Lock(2);

		// If an effect is taking over the vector field control
		if (m_fEffectTimer > 0 && m_nEffect != 4)
		{
			// Loop through each point in the vector field
			for (int a = 0; a < 128; a++)
			{
				for (int b = 0; b < 106; b++)
				{
					nMove = a;
					nMove += b * (128 * 2);

					// Swipe particles right
					if (m_nEffect == 0)
					{
						source[nMove * 4 + 0] = 0.5;
						source[nMove * 4 + 1] = 0;
						source[nMove * 4 + 2] = 0;

						source[(nMove + 128) * 4 + 0] = 0.5;
						source[(nMove + 128) * 4 + 1] = 0;
						source[(nMove + 128) * 4 + 2] = 0;
					}
					// Swipe particles left
					else if (m_nEffect == 1)
					{
						source[nMove * 4 + 0] = -0.5;
						source[nMove * 4 + 1] = 0;
						source[nMove * 4 + 2] = 0;

						source[(nMove + 128) * 4 + 0] = -0.5;
						source[(nMove + 128) * 4 + 1] = 0;
						source[(nMove + 128) * 4 + 2] = 0;
					}
					// Particles split left/right
					else if (m_nEffect == 2)
					{
						short nDir = -1;
						if (a > 64)
							nDir *= -1;
						source[nMove * 4 + 0] = 0.5 * nDir;
						source[nMove * 4 + 1] = 0;
						source[nMove * 4 + 2] = 0;

						source[(nMove + 128) * 4 + 0] = 0.5 * nDir;
						source[(nMove + 128) * 4 + 1] = 0;
						source[(nMove + 128) * 4 + 2] = 0;
					}
					// Particles converge to the center
					else if (m_nEffect == 3 && m_fEffectTimer > 0)
					{
						short nDir = 1;
						if (a > 64)
							nDir *= -1;
						source[nMove * 4 + 0] = 0.4 * nDir;
						source[nMove * 4 + 1] = 0;
						source[nMove * 4 + 2] = 0;

						source[(nMove + 128) * 4 + 0] = 0.4 * nDir;
						source[(nMove + 128) * 4 + 1] = 0;
						source[(nMove + 128) * 4 + 2] = 0;
					}
				}
			}

			// Increment timer
			m_fEffectTimer--;
		}
		// Standard operation of the vector field effect
		else
		{
			// Iterate through the Kinect depth buffer
			while (pBuffer < pBufferEnd)
			{
				// Get the current depth at this location
				USHORT depth = *pBuffer;

				// Calculate the X and Y values of this location
				x = (pBuffer - pBufferStart) % nWidth;
				y = (pBuffer - pBufferStart) / nWidth;

				// Reduce the values to the resolution of the vector field, UE4 vector fields are limited in their maximum size
				x = x / 4;
				y = y / 4;

				// Be sure we are not out of bounds
				if (x >= 0 && x < 128 && y >= 0 && y < 106)
				{
					// Reverse image
					x = 127 - x;
					y = 105 - y;

					// If we are within the reliable depth area, get the height
					if (depth > nMinDepth && depth <= nMaxDepth)
						fHeight = float(depth) / float(nMaxDepth);
					// Otherwise this point goes all the way to the back
					else
						fHeight = 1;

					// Step to the correct location in the vector field, which is stored as a one dimensional array
					nMove = x;
					nMove += y * (128 * 2);
					
					// The rear of the vector field, set to the height
					source[nMove * 4 + 0] = 0;
					source[nMove * 4 + 1] = fHeight;
					source[nMove * 4 + 2] = 0;
					
					// The front of the vector field, set opposite the height, as well as directional values
					// that push particles vertically and horizontally through the effect
					source[(nMove + 128) * 4 + 0] = m_fLeftRight;
					source[(nMove + 128) * 4 + 1] = (1 - fHeight) * -1;
					source[(nMove + 128) * 4 + 2] = m_fUpDown;

					// Ripple effect, using a sin wave based on time
					if (m_nEffect == 4)
					{
						source[nMove * 4 + 1] = source[nMove * 4 + 1] - (FMath::Sin(x + y + m_fEffectTimer) / 7);
						source[(nMove + 128) * 4 + 1] = source[(nMove + 128) * 4 + 1] + (FMath::Sin(x + y + m_fEffectTimer) / 6);
					}
				}

				// Step forward in the kinect buffer
				pBuffer += 4;

				// Step down the correct number of verticle lines
				if (x == 127)
					pBuffer += 3 * 512;
			}

			nCount = 0;
			fAudioScale = 0;

			// If the audio visualizer for this effect is enabled
			if (m_bAV == 1)
			{
				// Step through each desired frequency
				for (int a = 0; a < 128; a++)
				{
					fAudioScale = 0;

					// Total two frequencies
					for (int b = 0; b < 2; b++)
						fAudioScale += fft_fltr[(nCount++) * 2];

					// Average of two frequencies
					fAudioScale /= 2;

					// Step to the correct location in the vector field 1D array
					nMove = a;
					nMove += 53 * (128 * 2);

					// Add movement to the earlier set depth
					source[nMove * 4 + 2] = -5 *fAudioScale;

					// Step to the location one row lower in the vector field
					nMove = a;
					nMove += 52 * (128 * 2);

					// Add oposite movement to the depth
					source[nMove * 4 + 2] = 5 *fAudioScale;
				}
			}

			// Increment the effect timer
			if (m_nEffect == 4)
				m_fEffectTimer += 0.2;
		}

		// Unlock the vector field 
		m_pVectorField->SourceData.Unlock();
		
		// The following method only works in the editor
#if WITH_EDITOR
		// Post the property changed event, which triggers the GPU to reload the vector field
		for (TFieldIterator<UProperty> Property(UVectorFieldComponent::StaticClass()); Property; ++Property)
		{
			FPropertyChangedEvent PropertyChangedEvent(*Property);
			m_pVectorField->PostEditChangeProperty(PropertyChangedEvent);
		}
#else
		VectorFieldStatic->Resource->UpdateResource(VectorFieldStatic);
#endif
	}
}